import { ID, Field, ObjectType, GraphQLISODateTime } from 'type-graphql';
import User from './users.model';

// ======================================
//		Todo Entity - GraphQL
// ======================================
@ObjectType({ description: 'Todo Model' })
export default class Todo {
	@Field(() => ID)
	public id!: string;

	@Field(() => String)
	public description!: string;

	@Field(() => Boolean, { nullable: true, defaultValue: false })
	public check?: boolean;

	@Field(() => User, { nullable: true })
	public created_by?: User;

	@Field(() => User, { nullable: true })
	public updated_by?: User;

	@Field(() => GraphQLISODateTime, { nullable: true })
	public created_at?: string;

	@Field(() => GraphQLISODateTime, { nullable: true })
	public updated_at?: string;
}
