import { CreateTodoInput, RelationshipsTodoInput, UpdateTodoInput } from './inputs.resolver';
import TodosService from '../../../services/todos.service';
import { InjectRepository } from 'typeorm-typedi-extensions';
import PayloadResponse from '../../../config/payload';
import { Arg, ID, Resolver, Mutation } from 'type-graphql';
import Todo from '../../models/todos.model';

// ======================================
//				General
// ======================================
const TodoPayload = PayloadResponse(Todo);
type TodoPayload = InstanceType<typeof TodoPayload>;

@Resolver()
export default class TodosMutations {
	// ======================================
	//				Constructor
	// ======================================
	constructor(
		@InjectRepository(TodosService)
		private readonly service: TodosService,
	) {}

	// ======================================
	//			Created Todo Resolver
	// ======================================
	@Mutation(() => TodoPayload, {
		description: 'Created A New Todo Entity',
	})
	public async createTodo(
		@Arg('input', () => CreateTodoInput)
		input: CreateTodoInput,
		@Arg('relationships', () => RelationshipsTodoInput, {
			nullable: true,
		})
		relationships: RelationshipsTodoInput,
	) {
		return await this.service.created(input, relationships);
	}

	// ======================================
	//			Updated Todo Resolver
	// ======================================
	@Mutation(() => TodoPayload, { description: 'Updated A New Todo Entity' })
	public async updateTodo(
		@Arg('id', () => ID) id: number,
		@Arg('input', () => UpdateTodoInput) input: UpdateTodoInput,
		@Arg('relationships', () => RelationshipsTodoInput, {
			nullable: true,
		})
		relationships: RelationshipsTodoInput,
	) {
		return await this.service.updated(id, input, relationships);
	}

	// ======================================
	//		Deleted Todo Resolver
	// ======================================
	@Mutation(() => TodoPayload, {
		description: 'Deleted A Todo Entity',
	})
	public async deleteTodo(
		@Arg('id', () => ID) id: number,
		@Arg('relationships', () => RelationshipsTodoInput, {
			nullable: true,
		})
		relationships: RelationshipsTodoInput,
	) {
		return await this.service.deleted(id, relationships);
	}
}
