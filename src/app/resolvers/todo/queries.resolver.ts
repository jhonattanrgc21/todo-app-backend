import Todo from '../../models/todos.model';
import TodosService from '../../../services/todos.service';
import { InjectRepository } from 'typeorm-typedi-extensions';
import { ID, Arg, Query, Resolver } from 'type-graphql';

@Resolver()
export default class UsersQueries {
	// ======================================
	//				Constructor
	// ======================================
	constructor(
		@InjectRepository(TodosService)
		private readonly service: TodosService,
	) {}

	// ======================================
	//				Get All Todos
	// ======================================
	@Query(() => [Todo], { description: 'Get all Todos' })
	public async todos() {
		return await this.service.findAllTodo();
	}

	// ======================================
	//			Get All Todos Readys
	// ======================================
	@Query(() => [Todo], { description: 'Get all Todos Ready' })
	public async todosReady() {
		return await this.service.findAllTodoReady();
	}

	// ======================================
	//			Get All Todos No Readys
	// ======================================
	@Query(() => [Todo], { description: 'Get all Todos No Ready' })
	public async todosNoReady() {
		return await this.service.findAllTodoNoReady();
	}

	// ======================================
	//				Get Todo By ID
	// ======================================
	@Query(() => Todo, { description: 'Get Todo By ID' })
	public async todo(@Arg('id', () => ID) id: number) {
		return await this.service.findOneByID(id);
	}

	// ======================================
	//			Find Todo By Description
	// ======================================
	@Query(() => Todo, { description: 'Get Todo By Description' })
	public async todoByEmail(
		@Arg('description', () => String) description: string,
	) {
		return await this.service.findOneByDescription(description);
	}
}
