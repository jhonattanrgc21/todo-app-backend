import Relationships from '../../../config/relationship';
import { Field, InputType } from 'type-graphql';

// ======================================
//		Relationships Todo Input
// ======================================
@InputType({ description: 'Input To Relationships A Todo Entity' })
export class RelationshipsTodoInput extends Relationships() {}

// ======================================
//			Todo Input
// ======================================
@InputType({ description: 'Input To Create A Todo Entity' })
export class CreateTodoInput {
	@Field(() => String)
	public description: string;
}

@InputType({ description: 'Input To Update A Todo Entity' })
export class UpdateTodoInput {
	@Field(() => String, { nullable: true })
	public description?: string;

	@Field(() => Boolean, { nullable: true })
	public check?: boolean;
}
