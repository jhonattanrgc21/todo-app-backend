import Todo from "../../database/entities/todos.entity";

// ======================================
//			Todo Interface
// ======================================
export interface TodosInterface {
	description?: string;
	check?: boolean;
}

// ======================================
//		Relationships Todos
// ======================================
export interface RelationshipsTodos {
	created_by?: number;
	updated_by?: number;
}

// ======================================
//			Todo Payload
// ======================================
export interface TodoPayload {
	entity?: Todo;
	message?: string;
}

