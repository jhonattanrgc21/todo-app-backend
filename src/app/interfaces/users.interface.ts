import User from '../../database/entities/users.entity';

// ======================================
//			User Interface
// ======================================
export interface UsersInterface {
	email?: string;
	password?: string;
	first_name?: string;
	last_name?: string;
}

// ======================================
//			User Payload
// ======================================
export interface UserPayload {
	entity?: User;
	message?: string;
}
