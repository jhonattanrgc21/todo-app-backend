import Todo from '../database/entities/todos.entity';
import { Repository, EntityRepository, IsNull } from 'typeorm';
import {
	TodosInterface,
	RelationshipsTodos,
	TodoPayload,
} from '../app/interfaces/todo.interface';

@EntityRepository(Todo)
export default class TodosService extends Repository<Todo> {
	// ======================================
	//				Created User
	// ======================================
	public async created(
		input: TodosInterface,
		relationships: RelationshipsTodos,
	): Promise<TodoPayload> {
		const entity = this.create(input);
		entity.created_by = relationships.created_by
			? <any>{ id: relationships.created_by }
			: null;
		entity.updated_by = relationships.updated_by
			? <any>{ id: relationships.updated_by }
			: null;

		return {
			message: 'Registro guardado.',
			entity: await entity.save(),
		};
	}

	// ======================================
	//				Find All
	// ======================================
	public async findAllTodo(): Promise<Todo[]> {
		return await this.find({ relations: ['created_by', 'updated_by'] });
	}

	// ======================================
	//			Find All Todo Ready
	// ======================================
	public async findAllTodoReady(): Promise<Todo[]> {
		return await this.find({
			where: { check: true },
			relations: ['created_by', 'updated_by'],
		});
	}

	// ======================================
	//			Find All Todo No Ready
	// ======================================
	public async findAllTodoNoReady(): Promise<Todo[]> {
		return await this.find({
			where: { check: false },
			relations: ['created_by', 'updated_by'],
		});
	}

	// ======================================
	//			Find One By Description
	// ======================================
	public async findOneByDescription(
		description: string,
	): Promise<Todo | undefined> {
		return await this.findOne(
			{ description },
			{ relations: ['created_by', 'updated_by'] },
		);
	}

	// ======================================
	//			Find One By ID
	// ======================================
	public async findOneByID(id: number): Promise<Todo | undefined> {
		return await this.findOne(id, {
			relations: ['created_by', 'updated_by'],
		});
	}

	// ======================================
	//				Update
	// ======================================
	public async updated(
		id: number,
		input: TodosInterface,
		relationships: RelationshipsTodos,
	): Promise<TodoPayload> {
		const entity = await this.findOneOrFail(id, {
			relations: ['created_by', 'updated_by'],
		});
		entity.description = input.description
			? input.description
			: entity.description;
		entity.check = input.check ? input.check : entity.check;
		entity.updated_by = relationships.updated_by
			? <any>{ id: relationships.updated_by }
			: null;
		return {
			message: 'Registro actualizado.',
			entity: await this.save(entity),
		};
	}

	// ======================================
	//				Deleted
	// ======================================
	public async deleted(
		id: number,
		relationships: RelationshipsTodos,
	): Promise<TodoPayload> {
		const entity = await this.findOneOrFail(id, {
			relations: ['created_by', 'updated_by'],
		});
		entity.updated_by = relationships.updated_by
			? <any>{ id: relationships.updated_by }
			: entity.updated_by;

		const response = await this.save(entity);

		await this.delete(id);
		return {
			message: 'Registro eliminado.',
			entity: response,
		};
	}
}
