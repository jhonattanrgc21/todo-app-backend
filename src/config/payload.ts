import { ClassType, Field, ObjectType } from "type-graphql";

// ======================================
//			Payload - GraphQL
// ======================================
export default function PayloadResponse<TItem>(TItemClass: ClassType<TItem>) {
	@ObjectType(`${TItemClass.name}Payload`, {
		description: `${TItemClass.name} Payload`,
		isAbstract: true,
	})
	class PayloadResponseClass {
		@Field(() => TItemClass, { nullable: true })
		public entity?: TItem;

		@Field(() => String)
		public message!: string;
	}
	return PayloadResponseClass;
}
