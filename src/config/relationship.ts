import { ID, Field, InputType } from 'type-graphql';

// ======================================
//			Relationships - GraphQL
// ======================================
export default function Relationships<TItem>() {
	@InputType({ isAbstract: true })
	abstract class RelationshipsClass {
		@Field(() => ID, { nullable: true })
		public created_by?: number;

		@Field(() => ID, { nullable: true })
		public updated_by?: number;
	}
	return RelationshipsClass;
}
