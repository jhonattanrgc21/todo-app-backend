import {
	Index,
	Entity,
	Column,
	BeforeInsert,
	OneToMany
} from 'typeorm';
import bcrypt from 'bcryptjs';
import Todo from './todos.entity';
import UuidEntity from './uuid.entity';

// ======================================
//		User Entity - SQL
// ======================================
@Entity('users')
export default class User extends UuidEntity {
	@Index('users_email_unique', { unique: true })
	@Column({ type: 'varchar', length: 191 })
	public email!: string;

	@Column({ type: 'varchar', length: 191 })
	public password!: string;

	@Column({ type: 'varchar', length: 191, comment: 'Nombres.' })
	public first_name!: string;

	@Column({ type: 'varchar', length: 191, comment: 'Apellidos.' })
	public last_name!: string;

	// ======================================
	//			RelationShips
	// ======================================
	@OneToMany(() => Todo, (todos: Todo) => todos.created_by)
	public todos?: Todo[];

	// ======================================
	//			Encrypt Password
	// ======================================
	@BeforeInsert()
	public async encryptPassword() {
		const salt = await bcrypt.genSalt(10);
		this.password = await bcrypt.hash(this.password, salt);
	}

	// ======================================
	//			Match Password
	// ======================================
	public async matchPassword(receivedPassword: string) {
		return await bcrypt.compare(receivedPassword, this.password);
	}
}
