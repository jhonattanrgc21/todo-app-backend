import {
	Index,
	Entity,
	Column,
	ManyToOne,
	JoinColumn,
} from 'typeorm';
import User from './users.entity';
import UuidEntity from './uuid.entity';

// ======================================
//		Todo Entity - SQL
// ======================================
@Entity('todos')
export default class Todo extends UuidEntity {
	@Index('todos_description_unique', { unique: true })
	@Column({
		type: 'varchar',
		length: 191,
		comment: 'Descripcion de la tarea',
	})
	public description!: string;

	@Column({
		type: 'boolean',
		default: false,
		nullable: true,
		comment: 'para saber si la tarea fue realizada.',
	})
	public check?: boolean;

	// ======================================
	//			RelationShips
	// ======================================
	@ManyToOne(() => User, (user: User) => user.todos, { onUpdate: 'CASCADE', onDelete: 'CASCADE'})
	@JoinColumn({ name: 'created_by'})
	public created_by?: User;

	@ManyToOne(() => User, (user: User) => user.todos, { onUpdate: 'CASCADE', onDelete: 'CASCADE'})
	@JoinColumn({ name: 'updated_by' })
	public updated_by?: User;
}
