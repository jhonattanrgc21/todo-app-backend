import {
	BaseEntity,
	CreateDateColumn,
	UpdateDateColumn,
	PrimaryGeneratedColumn,
} from 'typeorm';

export default class UuidEntity extends BaseEntity {
	@PrimaryGeneratedColumn()
	public id!: number;

	@CreateDateColumn({ type: 'timestamptz', nullable: true })
	public created_at?: string;

	@UpdateDateColumn({ type: 'timestamptz', nullable: true })
	public updated_at?: string;
}
